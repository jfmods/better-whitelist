plugins {
    id("jfmod") version "1.6-SNAPSHOT"
}

allprojects { group = "io.gitlab.jfronny" }
base.archivesName = "better-whitelist"

val muscriptVersion = "1.8.0-SNAPSHOT"
jfMod {
    minecraftVersion = "1.21.4"
    yarn("build.1")
    loaderVersion = "0.16.9"
    libJfVersion = "3.18.3"
    fabricApiVersion = "0.110.5+1.21.4"

    modrinth {
        projectId = "better-whitelist"
        requiredDependencies.addAll("fabric-api", "libjf")
    }
}

dependencies {
    modImplementation("net.fabricmc.fabric-api:fabric-networking-api-v1")
    modImplementation("net.fabricmc.fabric-api:fabric-command-api-v2")
    include(modImplementation("io.gitlab.jfronny:muscript-all:$muscriptVersion")!!)
    modImplementation("io.gitlab.jfronny.libjf:libjf-base")

    // Dev env
    modLocalRuntime("io.gitlab.jfronny.libjf:libjf-devutil")
    modLocalRuntime("com.terraformersmc:modmenu:12.0.0-beta.1")
    // for modmenu
    modLocalRuntime("net.fabricmc.fabric-api:fabric-resource-loader-v0")
    modLocalRuntime("net.fabricmc.fabric-api:fabric-screen-api-v1")
    modLocalRuntime("net.fabricmc.fabric-api:fabric-key-binding-api-v1")
}
