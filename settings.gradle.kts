pluginManagement {
    repositories {
        maven("https://maven.frohnmeyer-wds.de/mirrors")
        gradlePluginPortal()
    }
}

rootProject.name = "better-whitelist"
