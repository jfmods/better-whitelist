package io.gitlab.jfronny.betterwhitelist.server;

import com.mojang.authlib.GameProfile;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;
import net.fabricmc.fabric.api.networking.v1.LoginPacketSender;
import net.fabricmc.fabric.api.networking.v1.PacketSender;

class Challenge {
    public final ManualFuture<?> challengeCompleted = new ManualFuture<>();
    public final ManualFuture<Dynamic> response = new ManualFuture<>();
    public final GameProfile profile;
    public LoginPacketSender sender;

    public Challenge(GameProfile profile, LoginPacketSender sender) {
        this.profile = profile;
        this.sender = sender;
    }
}
