package io.gitlab.jfronny.betterwhitelist;

import io.gitlab.jfronny.muscript.ast.DynamicExpr;
import io.gitlab.jfronny.muscript.ast.context.ExprUtils;
import io.gitlab.jfronny.muscript.core.MuScriptVersion;
import io.gitlab.jfronny.muscript.data.additional.DataExprMapper;
import io.gitlab.jfronny.muscript.data.additional.libs.StandardLib;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;
import io.gitlab.jfronny.muscript.parser.Parser;
import io.gitlab.jfronny.muscript.runtime.Runtime;
import io.gitlab.jfronny.muscript.serialize.Decompiler;

import static io.gitlab.jfronny.muscript.ast.context.ExprUtils.asDynamic;

public class DSerializer {
    public static String serialize(Dynamic d) {
        return Decompiler.decompile(DataExprMapper.map(d));
    }

    public static Dynamic deserialize(String s) {
        DynamicExpr dynamicExpr = asDynamic(Parser.parse(MuScriptVersion.DEFAULT, s));
        if (!ExprUtils.isDirect(dynamicExpr)) throw new RuntimeException("Invalid dynamic");
        return Runtime.evaluate(dynamicExpr, StandardLib.createScope(MuScriptVersion.DEFAULT));
    }
}
