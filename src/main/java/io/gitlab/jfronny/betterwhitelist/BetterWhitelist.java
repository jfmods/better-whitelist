package io.gitlab.jfronny.betterwhitelist;

import io.gitlab.jfronny.betterwhitelist.packet.ChallengePacket;
import io.gitlab.jfronny.betterwhitelist.packet.ChallengeResponsePacket;
import io.gitlab.jfronny.betterwhitelist.packet.HandshakePacket;
import io.gitlab.jfronny.commons.logger.SystemLoggerPlus;
import io.gitlab.jfronny.muscript.core.MuScriptVersion;
import io.gitlab.jfronny.muscript.data.additional.DFinal;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;
import io.gitlab.jfronny.muscript.data.additional.libs.StandardLib;
import io.gitlab.jfronny.muscript.data.dynamic.*;
import net.fabricmc.fabric.api.networking.v1.PayloadTypeRegistry;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.metadata.ModDependency;
import net.fabricmc.loader.api.metadata.ModMetadata;
import net.minecraft.util.Identifier;

import java.util.Map;
import java.util.stream.Collectors;

import static io.gitlab.jfronny.muscript.data.additional.DFinal.of;

public class BetterWhitelist {
    public static final String MOD_ID = "better-whitelist";
    public static final SystemLoggerPlus LOG = SystemLoggerPlus.forName(MOD_ID);
    public static final Identifier HANDSHAKE_CHANNEL = Identifier.of(MOD_ID, "handshake");
    public static final Identifier CHALLENGE_CHANNEL = Identifier.of(MOD_ID, "challenge");
    public static final int PROTOCOL_VERSION = 1;
    public static final ModMetadata MOD_METADATA = FabricLoader.getInstance().getModContainer(MOD_ID).orElseThrow().getMetadata();
    public static final Scope SCOPE = StandardLib.createScope(MuScriptVersion.DEFAULT);

    static {
        SCOPE.set("mods", FabricLoader.getInstance()
                .getAllMods()
                .stream()
                .collect(Collectors.toMap(
                        a -> a.getMetadata().getId(),
                        BetterWhitelist::wrap
                ))
        ).set("mod", args -> {
            if (args.size() != 1) throw new IllegalArgumentException("Invalid number of arguments for mod: expected 1 but got " + args.size());
            return FabricLoader.getInstance()
                    .getModContainer(args.get(0).asString().getValue())
                    .<Dynamic>map(BetterWhitelist::wrap)
                    .orElse(new DNull());
        }).set("println", args -> {
            String s = args.isEmpty() ? "" : args.size() == 1 ? args.get(0).asString().getValue() : args.toString();
            System.out.println(s);
            return of(s);
        });
    }

    public static void initialize() {
        // Initialization for things like registering packet types would go here
    }

    private static DObject wrap(ModContainer mod) {
        return of(Map.of(
                "id", of(mod.getMetadata().getId()),
                "name", of(mod.getMetadata().getName()),
                "description", of(mod.getMetadata().getDescription()),
                "authors", of(mod.getMetadata().getAuthors().stream().map(s -> of(s.getName())).toList()),
                "contributors", of(mod.getMetadata().getContributors().stream().map(s -> of(s.getName())).toList()),
                "version", of(mod.getMetadata().getVersion().getFriendlyString()),
                "environment", of(switch (mod.getMetadata().getEnvironment()) {
                    case CLIENT -> "client";
                    case SERVER -> "server";
                    case UNIVERSAL -> "*";
                }),
                "license", of(mod.getMetadata().getLicense().stream().map(DFinal::of).toList()),
                "contact", of(mod.getMetadata().getContact().asMap().entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, a -> of(a.getValue())))),
                "depends", of(mod.getMetadata().getDependencies().stream().filter(v -> v.getKind() == ModDependency.Kind.DEPENDS).map(s -> of(s.getModId())).toList())
        ));
    }
}
