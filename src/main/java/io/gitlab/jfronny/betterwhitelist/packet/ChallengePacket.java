package io.gitlab.jfronny.betterwhitelist.packet;

import io.gitlab.jfronny.betterwhitelist.BetterWhitelist;
import io.netty.buffer.ByteBuf;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.network.codec.PacketCodecs;
import net.minecraft.network.packet.CustomPayload;

import java.util.List;

public record ChallengePacket(String challenge, List<String> params) implements CustomPayload {
    public static final Id<ChallengePacket> ID = new CustomPayload.Id<>(BetterWhitelist.CHALLENGE_CHANNEL);
    public static final PacketCodec<PacketByteBuf, ChallengePacket> CODEC = PacketCodec.tuple(
            PacketCodecs.STRING, ChallengePacket::challenge,
            PacketCodecs.<ByteBuf, String>toList().apply(PacketCodecs.STRING), ChallengePacket::params,
            ChallengePacket::new);

    @Override
    public Id<? extends CustomPayload> getId() {
        return ID;
    }
}
