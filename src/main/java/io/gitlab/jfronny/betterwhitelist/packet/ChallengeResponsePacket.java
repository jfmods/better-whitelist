package io.gitlab.jfronny.betterwhitelist.packet;

import io.gitlab.jfronny.betterwhitelist.BetterWhitelist;
import io.netty.buffer.ByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.network.codec.PacketCodecs;
import net.minecraft.network.packet.CustomPayload;

public record ChallengeResponsePacket(String message) implements CustomPayload {
    public static final CustomPayload.Id<ChallengeResponsePacket> ID = new CustomPayload.Id<>(BetterWhitelist.CHALLENGE_CHANNEL);
    public static final PacketCodec<ByteBuf, ChallengeResponsePacket> CODEC = PacketCodecs.STRING.xmap(ChallengeResponsePacket::new, ChallengeResponsePacket::message);

    @Override
    public Id<? extends CustomPayload> getId() {
        return ID;
    }
}
