package io.gitlab.jfronny.betterwhitelist.packet;

import io.gitlab.jfronny.betterwhitelist.BetterWhitelist;
import io.netty.buffer.ByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.network.codec.PacketCodecs;
import net.minecraft.network.packet.CustomPayload;

public record HandshakePacket(int version) implements CustomPayload {
    public static final CustomPayload.Id<HandshakePacket> ID = new CustomPayload.Id<>(BetterWhitelist.HANDSHAKE_CHANNEL);
    public static final PacketCodec<ByteBuf, HandshakePacket> CODEC = PacketCodecs.INTEGER.xmap(HandshakePacket::new, HandshakePacket::version);

    @Override
    public Id<? extends CustomPayload> getId() {
        return ID;
    }
}
