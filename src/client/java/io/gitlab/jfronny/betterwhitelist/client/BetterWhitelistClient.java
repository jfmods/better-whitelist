package io.gitlab.jfronny.betterwhitelist.client;

import io.gitlab.jfronny.betterwhitelist.BetterWhitelist;
import io.gitlab.jfronny.betterwhitelist.DSerializer;
import io.gitlab.jfronny.betterwhitelist.packet.ChallengePacket;
import io.gitlab.jfronny.betterwhitelist.packet.HandshakePacket;
import io.gitlab.jfronny.muscript.core.MuScriptVersion;
import io.gitlab.jfronny.muscript.data.additional.DFinal;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;
import io.gitlab.jfronny.muscript.data.dynamic.DCallable;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;
import io.gitlab.jfronny.muscript.parser.Parser;
import io.gitlab.jfronny.muscript.runtime.Runtime;
import net.fabricmc.api.*;
import net.fabricmc.fabric.api.client.networking.v1.ClientLoginNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.client.MinecraftClient;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.TextContent;

import java.util.*;
import java.util.concurrent.CompletableFuture;

import static io.gitlab.jfronny.muscript.ast.context.ExprUtils.asDynamic;

@Environment(EnvType.CLIENT)
public class BetterWhitelistClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        BetterWhitelist.initialize();

        ClientLoginNetworking.registerGlobalReceiver(BetterWhitelist.HANDSHAKE_CHANNEL, (client, handler, buf, listenerAdder) -> {
            // buf also contains the server version
            PacketByteBuf resultBuf = PacketByteBufs.create();
            HandshakePacket.CODEC.encode(resultBuf, new HandshakePacket(BetterWhitelist.PROTOCOL_VERSION));
            return CompletableFuture.completedFuture(resultBuf);
        });

        ClientLoginNetworking.registerGlobalReceiver(BetterWhitelist.CHALLENGE_CHANNEL, (client, handler, buf, listenerAdder) -> {
            ChallengePacket packet = ChallengePacket.CODEC.decode(buf);
            Scope fork = BetterWhitelist.SCOPE.fork();
            fork.set("resourcePacks", MinecraftClient.getInstance()
                    .getResourcePackManager()
                    .getEnabledProfiles()
                    .stream()
                    .map(s -> DFinal.of(Map.of(
                            "name", DFinal.of(s.getId()),
                            "displayName", DFinal.of(toString(s.getDisplayName().getContent())),
                            "description", DFinal.of(toString(s.getDescription().getContent()))
                    )))
                    .toList());
            BetterWhitelist.LOG.info("Received challenge: " + packet.challenge());
            DCallable script = Runtime.evaluate(asDynamic(Parser.parse(MuScriptVersion.DEFAULT, packet.challenge())), fork).asCallable();
            List<Dynamic> params = new LinkedList<>();
            for (String param : packet.params()) {
                params.add(Runtime.evaluate(asDynamic(Parser.parse(MuScriptVersion.DEFAULT, param)), fork));
            }
            String resultString = DSerializer.serialize(script.call(DFinal.of(params)));
            BetterWhitelist.LOG.info("Sending result: " + resultString);
            PacketByteBuf resultBuf = PacketByteBufs.create();
            resultBuf.writeString(resultString);
            return CompletableFuture.completedFuture(resultBuf);
        });
    }

    private String toString(TextContent text) {
        StringBuilder sb = new StringBuilder();
        text.visit(asString -> {
            sb.append(asString);
            return Optional.empty();
        });
        return sb.toString();
    }
}
