Better-Whitelist provides a basic whitelist system for your server that you can adapt to your use case.

The mod is required both on the server and any clients connecting to the server.

The whitelist is configured using [μScript](https://git.frohnmeyer-wds.de/Johannes/java-commons/src/branch/master/muscript), but you don't have to be good at programming to use this mod.

A config file with some additional details will be created when you first start your server with the mod.

A simple config for Better-Whitelist could look as follows (please note that you'd probably have to add exceptions for client-side mods with optional server-side components when using this):
```
// convert a mod to a simpler representation to save bandwidth
simplify = { mod -> { id = mod.id, version = mod.version } }

// all non-serverside mods present on the server
sharedMods = mods::values()
  ::filter({ v -> v.environment != 'server' & v.id != 'java' })
  ::map(simplify)

// ensure the client has the correct version of every non-serverside mod on the server
clientMissing = challenge({ arg ->
  arg::filter({ v -> !mods::values()::anyMatch({ m -> v.id == m.id & v.version == m.version }) })
}, sharedMods)
assert(clientMissing::isEmpty(), 'You lack required mods: ' || clientMissing)

// ensure the client has no additional mods that would be required on the server
clientAdditional = challenge({ arg, fn ->
  clientSideMods = mods::values()::filter({ v -> v.environment != 'client' & v.id != 'java' })
  clientSideMods::filter({ v -> !arg::anyMatch({ m -> v.id == m.id & v.version == m.version }) })::map(fn)
}, sharedMods, simplify)
assert(clientAdditional::isEmpty(), 'You have unsupported mods: ' || clientAdditional)

// filter resource packs for X-Ray packs
bannedWords = listOf('xray', 'x-ray', 'cheat')
assert(!challenge({ ->
  resourcePacks::map({ pack -> pack.name || ' ' || pack.displayName || ' ' || pack.description })
})::anyMatch({ v -> bannedWords::anyMatch({ word -> v::toLower()::contains(word) }) }), "Please don't cheat, " || user.name)
```

The following things are available to your scripts:
- All methods from the μScript standard library (including date/time, meaning you can temporarily whitelist or blacklist users or create countdowns)
- `resourcePacks` (client only): a list of resource packs, each of which has a `name`, `displayName` and `description`
- `println('message')` (both): a function for debugging
- `mods` (both): a list of loaded mods with the same fields as the result of `mod()`
- `mod('id')` (both): information about a specific mod (or null): `id`, `name`, `description`, `authors` (list of strings), `contributors` (list of strings), `version`, `environment` (either `client`, `server` or `*`), `license`, `contact` (same as FMJ), `depends` (list of strings)
- `assert(bool)` (server): assert that something is true and kick the player if it's not (optionally, add a message as the second parameter)
- `challenge({->closure}, additional arguments...)` (server): send a closure to the client to be executed there. The arguments may contain other closures.
- `user` (server): information about the user trying to log in, namely their `id` and `name`